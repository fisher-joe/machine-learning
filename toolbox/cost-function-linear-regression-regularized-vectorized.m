function cost = cost(theta, X, y, lambda)

[m, n] = size(X);

% Create a pseudo-identity function, for purposes of not regularizing theta(0)
L = eye(n);
L(1,1) = 0;

% Vectorized! Boom.
cost = 1/(2*m) * (sum((theta'*X' - y').^2) + lambda * sum((theta'*L).^2));

endfunction

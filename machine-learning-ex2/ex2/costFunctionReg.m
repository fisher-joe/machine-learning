function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

[m, n] = size(X);

onesies = ones(m,1);

h = sigmoid(X*theta);

% I love you, L! (exclamatory; not "L-factorial")
L = eye(n);
L(1,1) = 0;

% so easy now!
reg_term = lambda/(2*m) * sum((theta'*L).^2);

J = 1/m * sum((-y .* log(h)) - ((onesies - y) .* log(onesies - h))) + reg_term;

% multiplying by L gives us an nx1 vector of the lambda terms,
% but it zeros out the theta(0) one! (Thanks, buddy!)
grad = 1/m * ((h - y)' * X)' + L * (lambda/m * theta);

% =============================================================

end

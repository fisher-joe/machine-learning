function theta = f(X, y, lambda)
  [m, n] = size(X);
  L = eye(n);        % note:  this is nxn instead of (n+1)x(n+1) because it's
                     %        assumed we've already added the intercept term
                     %        (feature of 1's, i.e. theta(0))
  L(1,1) = 0;
  theta = pinv(X'*X + lambda*L) * X'*y;
endfunction


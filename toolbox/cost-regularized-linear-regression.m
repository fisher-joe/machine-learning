% Computes the cost function of linear regression using regularization.
function cost = cost(theta, X, y, lambda)

[m, n] = size(X);
L = eye(n);
L(1,1) = 0;
cost = 1/(2*m) * (sum((theta'*X' - y').^2) + lambda * sum((theta'*L).^2));

endfunction

% Computes the gradient (only; doesn't multiply by alpha) for linear regression
% using regularization.
function grad = grad(theta, X, y, lambda)

[m, n] = size(X);

x_zero = X(:,1);
X_Rest = X(:,2:n);
theta_rest = theta(2:n);

% this could also be done on one line by multiplying (lambda/m * theta) by "L"
% (see cost-regularized-linear-regression.m)
grad_zero = 1/m * (X*theta - y)' * x_zero;
grad_rest = (1/m * ((X*theta - y)' * X_Rest))' + lambda/m * theta_rest;

grad = [grad_zero; grad_rest];

endfunction

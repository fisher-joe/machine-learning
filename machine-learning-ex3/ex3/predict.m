function p = predict(Theta1, Theta2, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(Theta2, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned neural network. You should set p to a 
%               vector containing labels between 1 to num_labels.
%
% Hint: The max function might come in useful. In particular, the max
%       function can also return the index of the max element, for more
%       information see 'help max'. If your examples are in rows, then, you
%       can use max(A, [], 2) to obtain the max for each row.
%

% size(Theta1) == [25 401]
% size(Theta2) == [10 26]

% given a1 & Theta1, compute a2
% given a2 & Theta2, compute a3 (i.e. h)
% want a vector of m predictions (h(x)'s)

% vectorized implementation
A1 = [ones(1,m); X'];           % examples in column vectors, with bias added

Z2 = Theta1 * A1;               % size(Z2) = [25 m] (act'ns per example)
A2 = 1 ./ (1 + exp(-1.*Z2));    % "-Z2" should also work...

A2 = [ones(1,m); A2];           % add m number of bias units

Z3 = Theta2 * A2;               % size(Z3) = [10 m]
A3 = 1 ./ (1 + exp(-1.*Z3));    % A3(i,:) = column vector answer for h(x)

% this is good enough for now -- need the INDEXES of the row with the 1...
for i=1:m
    [val, ival] = max(A3(:,i));
    p(i) = ival;
end

% =========================================================================


end

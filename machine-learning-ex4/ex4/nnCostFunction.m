function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% size(X)      = [5000 400]
% size(Theta1) = [25 401]
% size(Theta2) = [10 26]
% size(y)      = [5000 1]

% turn y into a matrix Y of classification vectors (the tricky way)
% Y will be 5000x10
eye_matrix = eye(num_labels);
Y = eye_matrix(y,:);

% First, do forward propagation
% You'll then have your h(x)'s (in A3) for plugging in above
A1 = [ones(m,1) X];             % size(A1) = [m 401]
Z2 = A1 * Theta1';              % size(Z2) = [m 25] (row = activ'ns per example)
A2 = 1 ./ (1 + exp(-1.*Z2));
A2 = [ones(m,1) A2];            % add m number of bias units
Z3 = A2 * Theta2';              % size(Z3) = [m 10]
A3 = 1 ./ (1 + exp(-1.*Z3));    % A3(i,:) = row vector answer for h(x)

% Compute the (unregularized) cost function. J should always be a scalar!
% (the double-sum here does what we want because I don't know
%   (presuming the inner one decides to sum on the columns, which would be the
%   labels, and the outer one sums on the rows (features)?))
J = 1/m * sum(sum(-Y.*log(A3) - (1 - Y).*log(1 - A3)));

% Now do regularization
J = J + lambda/(2*m) * (sum(sum((Theta1(:,2:end).^2),2),1) + sum(sum((Theta2(:,2:end).^2),2),1));

%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%

d3 = A3 - Y;
d2 = (d3 * Theta2(:,2:end)) .* sigmoidGradient(Z2);

D1 = d2' * A1;  % size(D1) = [25 401]
D2 = d3' * A2;  % size(D2) = [10 26]

Theta1_grad = 1/m .* D1;
Theta2_grad = 1/m .* D2;

% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% zero out the bias unit columns, so that we can add the regularization factor
% without needing to resort to an "if" statement
% (this is OK since these are local variables...)
Theta1(:,1) = 0;
Theta2(:,1) = 0;

Theta1_grad = Theta1_grad + lambda/m * Theta1;
Theta2_grad = Theta2_grad + lambda/m * Theta2;

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end

function p = predict(theta, X)
%PREDICT Predict whether the label is 0 or 1 using learned logistic 
%regression parameters theta
%   p = PREDICT(theta, X) computes the predictions for X using a 
%   threshold at 0.5 (i.e., if sigmoid(theta'*x) >= 0.5, predict 1)

m = size(X, 1); % Number of training examples

% You need to return the following variables correctly
p = zeros(m, 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the following code to make predictions using
%               your learned logistic regression parameters. 
%               You should set p to a vector of 0's and 1's
%

p = sigmoid(X*theta);

% h(x) = P(y=1|x;theta); P(y=0|x;theta) = 1 - P(y=1|x;theta)
% p(i,j) = floor(p(i,j) + 0.5)
adjust = ones(m,1) * 0.5;

% adjust each probability in P and take the floor, so they end up being
% 0's or 1's
p = floor(p + adjust);

% =========================================================================


end

function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%

%%% vectorized! %%%

m = size(X,1);
n = size(X,2);

distances = zeros(m,K);
diff = zeros(m,n);
dist = zeros(m,1);

for k=1:K
    % calc diff of this centroid against all examples in X
    diff = bsxfun(@minus, X, centroids(k,:));   % manual broadcasting
    % calc the 2-norm for all rows at the same time
    dist = norm(diff, 2, "rows");
    distances(:,k) = dist;
end

for i=1:m
    [dontcare, idx(i)] = min(distances(i,:));
end

% =============================================================

end


function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%
%ctries = [0.01; 0.03; 0.1; 0.3; 1; 3; 10; 30];
%sigmatries = ctries;

% surely we'll beat 100% error :)
%min_err = 1;

%for i=1:length(ctries)
%    for j=1:length(sigmatries)
        
        % train up our model based on some combination of C and sigma
%        model = svmTrain(X, y, ctries(i), @(x1, x2) gaussianKernel(x1, x2, sigmatries(j)));

        % verify our model against the cross-validation set
%        predictions = svmPredict(model, Xval);

        % compute error between our predictions & yval (how'd our CV set do?)
%        err = mean(double(predictions ~= yval));

        % have we found our best error yet?
%        if (err < min_err)
%            min_err = err;
%            C = ctries(i);
%            sigma = sigmatries(j);
%        end
%    end
%end

% these are the values the above training found
% (commented out the above so submissions wouldn't take so long...)
C = 1;
sigma = 0.1;

% =========================================================================

end
